#!/usr/bin/env python3
import random, re, sys
from collections.abc import Iterable

space = re.compile(r"\W+")

def shuffle_str(data: str, random: random.Random) -> Iterable[str]:
    # print("shuffle_str", repr(data), flush=True, file=sys.stderr)
    if len(data) == 2:
        return reversed(data)
    return random.sample(data, k=len(data))

def replace_word(word: str, random: random.Random) -> Iterable[str]:
    # print("replace_word", repr(word), flush=True, file=sys.stderr)
    if len(word) < 2:
        yield word
    else:
        yield word[0]
        yield from shuffle_str(word[1:-1], random)
        yield word[-1]
    yield ""

def replace(data: Iterable[str], random: random.Random) -> Iterable[str]:
    for chunk in data:
        while m := space.search(chunk):
            if before := chunk[:m.start()]:
                yield from replace_word(before, random)
            yield m.group(0)
            chunk = chunk[m.end():]
        if chunk:
            yield from replace_word(chunk, random)

def main(seed: str | None = None) -> int | str:
    # print(f"{seed=}", file=sys.stderr, flush=True)
    rand = random.Random(seed) if seed else random.SystemRandom()
    for _ in replace(sys.stdin, rand):
        if not _:
            sys.stdout.flush()
            # __import__("time").sleep(0.1)
            continue
        sys.stdout.write(_)
    sys.stdout.flush()
    return 0

if __name__ == "__main__":
    sys.exit(main(" ".join(sys.argv[1:]) or None))
